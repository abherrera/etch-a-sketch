// Ugly globals variable
let clickNumber = 1;
let isMousedownInGrid = false;

// functions to create the grid
function createDivsTo(numberOfDivs, parentElement) {
    for (let i = 0; i < numberOfDivs; i++) {
        let gridRow = document.createElement("div");
        parentElement.append(gridRow);
    }
}

function changeCellsSize(cells, cellSize) {
    cells.forEach(function(cell) {
        cell.style.width = `${cellSize}px`;
        cell.style.height = `${cellSize}px`;
    });
}

function addStyleToGrid(grid, columns) {
    grid.style.gridTemplateColumns = `repeat(${columns}, min-content)`;
    const gridWidth = parseFloat(getComputedStyle(grid).width);
    let cells = grid.childNodes;
    let cellSize = gridWidth/columns;
    changeCellsSize(cells, cellSize);
}

function createGrid(size) {
    const grid = document.querySelector(".grid");
    createDivsTo(size*size, grid);
    addTagsToGrid(grid);
    addStyleToGrid(grid, size);
    addListenersForClicking();
    addDragGrid();
}

function addClassToChildren(parentElement, tagName) {
    parentElement.childNodes.forEach(childElement => childElement.className = tagName)
}

function addTagsToGrid(grid) {
    addClassToChildren(grid, "cell");
}

// Paint cell

function randomBetween(min, max) {
    return min + Math.floor(Math.random() * (max - min + 1));
}

function fillWithRandomColorGradient(element) {
    const gradient = clickNumber/10;
    if (clickNumber === 10) {
        clickNumber = 0;
        element.style.backgroundColor = "black";
    } else {
        // Repeated code... but is better than returning an array.
        const r = randomBetween(0, 255);
        const g = randomBetween(0, 255);
        const b = randomBetween(0, 255);
        element.style.backgroundColor = `rgba(${r},${g},${b},${gradient})`;
    }
    clickNumber++;
}

// Listener

function fillWithRandomColorGradientIfMouseDown() {
    if (isMousedownInGrid) {
        fillWithRandomColorGradient(this);
    }
}

function addListenersForClicking() {
    let cells = document.querySelectorAll(".cell");
    cells.forEach(cell => cell.addEventListener("click", function() {
        fillWithRandomColorGradient(this);
    }));
}

function addDragGrid() {
    const grid = document.querySelector(".grid");
    grid.addEventListener("mousedown", function () {
        isMousedownInGrid = true;  
    });
    grid.addEventListener("mouseup", function () {  
        isMousedownInGrid = false;
    });
    // Color if drag
    let cells = document.querySelectorAll(".cell");
    cells.forEach(cell => cell.addEventListener("mouseover", fillWithRandomColorGradientIfMouseDown));
}

// restart board
function deleteGrid() {
    const grid = document.querySelector('.grid');
    while (grid.firstChild) {
      grid.removeChild(grid.lastChild);
    }
}

function clearGrid() {
    let cells = document.querySelectorAll(".cell");
    cells.forEach(cell => cell.style.backgroundColor = "white");
}

function restartGrid() {
    clearGrid();

    let newGridSize = parseInt(prompt("Enter a new size between 1 and 100: "));
    if (!newGridSize || newGridSize < 1 || newGridSize > 100) {
        alert("Invalid new size: should be between 1 and 100");
        return;
    } 
    deleteGrid();
    createGrid(newGridSize, newGridSize);
}

function linkResetButton() {
    const reset = document.getElementById("clear");
    reset.addEventListener("click", restartGrid);
}

// Main
createGrid(16);
linkResetButton();